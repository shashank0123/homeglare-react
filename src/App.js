import React from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom'

function App() {
  return (
    <Router>
    <div className="app">
      <Switch>
         <Route path="/login"><h1>login</h1></Route>
         <Route path="/register"><h1>register</h1></Route>
         <Route path="/category/:slug"><h1>category</h1></Route>
         <Route path="/product_details/:slug"><h1>product_details/:slug</h1></Route>
         <Route path="/wishlist"><h1>wishlist</h1></Route>
         <Route path="/cart"><h1>cart</h1></Route>
         <Route path="/checkout"><h1>checkout</h1></Route>
         <Route path="/my_accounts"><h1>my_accounts</h1></Route>
         <Route path="/my_order"><h1>my_order</h1></Route>
         <Route path="/my_addresses"><h1>my_addresses</h1></Route>
         <Route path="/create_address"><h1>create_address</h1></Route>
         <Route path="/faq"><h1>faq</h1></Route>
         <Route path="/return_policy"><h1>return_policy</h1></Route>
         <Route path="/refund_policy"><h1>refund_policy</h1></Route>
         <Route path="/about_us"><h1>about_us</h1></Route>
         <Route path="/contact_us"><h1>contact_us</h1></Route>
         <Route path="/"><h1>Home</h1></Route>
      </Switch>
    </div>
    </Router>
  );
}

export default App;
